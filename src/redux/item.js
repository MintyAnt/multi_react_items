
import React from 'react';

var Item = React.createClass({
    handleChangeName: function(e) {
        var item = {name: event.target.value, value: this.props.value, purchaser: this.props.purchaserName};
        this.props.editItem(this.props.index, item);
    },
    removeMe: function(e) {
        e.preventDefault();
        this.props.removeItem(this.props.index);
    },
    render: function() {
        return (
            <div id="item">
                <input type="text" defaultValue={this.props.name} onChange={this.handleChangeName} />
                <form onSubmit={this.removeMe}>
                    <button>-</button>
                </form>
            </div>
        );
    }
});

module.exports = Item;