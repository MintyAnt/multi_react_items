
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import App from './app.js';

var store = createStore(function(state={items: ['1','2','3','4']}, action) {
    switch (action.type) {
        case 'REMOVE_ITEM':
            var newItems = state.items.filter((thing, index) => index != action.index);
            return { ...state, items: newItems };
        case 'EDIT_ITEM':
            var newItems = items.slice();
            newItems[action.index] = action.item;
            return {
                ...state,
                items: newItems
            };
        default: return state;
    }
});

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('items_test')
);