
import React from 'react';
import { connect } from 'react-redux';
import Item from './item.js';

var App = React.createClass({
    render: function() {
        var self = this;
        return (
            <div id="itemsList">
                { this.props.items.map(function(item, i) {
                    return <Item name={item}
                                editItem={self.props.editItem}
                                removeItem={self.props.removeItem} 
                                index={i}
                                key={i} />;
                })}
                <div>{this.props.items.map(function(item, i) {
                    return <p key={i}>{i} : {item}</p>;
                })}</div>
            </div>
        );
    }
});

export default connect(
    (state) => ({ items: state.items }),
    (dispatch) => ({
        removeItem: (index) => dispatch({type: 'REMOVE_ITEM', index: index}),
        editItem: (index, newItem) => dispatch({type: 'EDIT_ITEM', item: newItem, index: index})
    })
)(App);

module.exports = App;