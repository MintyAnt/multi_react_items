
import React from 'react';
import ReactDOM from 'react-dom';

var App = React.createClass({
    getInitialState: function() {
        return { items: ['1', '2', '3', '4'] };
    },
    removeSecond: function(e) {
        e.preventDefault();
        this.removeItem(1);
    },
    editItem: function(index, newItem) {
        var newItems = this.state.items.slice();
        newItems[index] = newItem;
        this.setState({items: newItems});
    },
    removeItem: function(index) {
        var newItems = this.state.items;
        newItems.splice(index, 1);
        this.setState({items: newItems});
    },
    render: function() {
        var self = this;
        return (
            <div id='itemsList'>
                { this.state.items.map( function(item, i) {
                    return <Item name={item} 
                        index={i} 
                        key={i} 
                        editItem={self.editItem}
                        removeItem={self.removeItem} />;
                })}
                <form onSubmit={this.removeSecond}>
                    <button>Remove 2nd</button>
                </form>
                <div>{this.state.items.map(function(item, i) {
                    return <p key={i}>{i} : '{item}'</p>;
                })}</div>
            </div>
        );
    }
});

var Item = React.createClass({
    handleChangeName: function(e) {
        this.props.editItem(this.props.index, e.target.value);
    },
    render: function() {
        return (
            <div id="item">
                <input type="text" defaultValue={this.props.name} onChange={this.handleChangeName} />
            </div>);
    }
});

ReactDOM.render(
    <App />,
    document.getElementById('items_test')
);